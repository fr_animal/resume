import * as React from 'react';
import * as ReactDOMServer from 'react-dom/server';
import * as cheerio from 'cheerio'
import { readFileSync } from 'fs';
import App from "../src/App";

const html = readFileSync('../public/index.html').toString();

const $ = cheerio.load(html);
$('#root').append(ReactDOMServer.renderToString(<App />))
