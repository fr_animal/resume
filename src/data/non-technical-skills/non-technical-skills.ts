import { type NonTechnicalSkills } from './types'

export const nonTechnicalSkills: NonTechnicalSkills = [
  'Engaging with Product Owners and Business Analysts, asking enough questions to gather thoroughly defined requirements to bring to technical discussions/refinements, avoiding abiguities after implementation has already begun.',
  'Ensuring technical knowledge is spread across the team, breaking down knowledge siloing.',
  'Providing high level technical explanations to Product Owners to help prioritise features.',
  'Performing technical investigations to inform the offering of alternative solutions, and/or slicing of large feautres to Product Owners in order to deliver features incrementally',
  'Working with stakeholders in order to allow for balancing of delivering user value quickly, with technical excellence.',
  'Mentoring & pair programming with the more junior members of the team.',
  'Working with Product Owners and Business Analysts to ensure that user stories are detailed enough to provide full understand to technical team, '
]
