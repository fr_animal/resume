type NonTechnicalSkill = string

export type NonTechnicalSkills = NonTechnicalSkill[]
