export interface Education {
  institution: string
  qualification: string
}

export type Educations = Education[]
