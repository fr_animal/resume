export const plainTextToClassname = (text: string): string =>
  text.replace(/ /g, '-').toLowerCase()
